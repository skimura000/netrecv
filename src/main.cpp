#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <cstdarg>

#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>

#include "utils.hpp"

class CommandlineError : public ApplicationError {
public:
    using ApplicationError::ApplicationError;
    using ApplicationError::what;
};

class UnknownError : public ApplicationError {
public:
    using ApplicationError::ApplicationError;
    using ApplicationError::what;
};

static void print_usage(void)
{
    fprintf(stderr, "Usage: netrecv ip-address port [file]\n");
}

static std::string HostName;
static std::string Port;
static std::string FileName;

static void parse_args(int argc, char *argv[])
{
    if (argc < 3) {
        THROW(CommandlineError, "too few arguments.");
    }
    if (argc > 4) {
        THROW(CommandlineError, "too many arguments.");
    }
    HostName = argv[1];
    Port = argv[2];
    if (argc == 4) {
        FileName = argv[3];
    }else{
        FileName = "";
    }
}

static HANDLE OpenOutputFile(const char *fname)
{
    HANDLE ret;
    if (*fname == '\0') {
        ret = GetStdHandle(STD_OUTPUT_HANDLE);
        if (ret == INVALID_HANDLE_VALUE) {
            THROW_WIN32API_ERROR(GetStdHandle);
        }
    }else{
        ret = CreateFileA(FileName.c_str(),
                          GENERIC_WRITE,
                          0,
                          0,
                          CREATE_ALWAYS,
                          FILE_ATTRIBUTE_NORMAL,
                          0);
        if (ret == INVALID_HANDLE_VALUE) {
            THROW_WIN32API_ERROR(CreateFileA);
        }
    }
    return ret;
}

static SOCKET Listen(const char *hostname, const char *port)
{
    ADDRINFO addrhint, *addr;
    memset(&addrhint, 0, sizeof(addrhint));
    addrhint.ai_socktype = SOCK_STREAM;
    addrhint.ai_family = AF_INET;
    if (getaddrinfo(hostname, port, &addrhint, &addr) != 0) {
        THROW_WIN32API_ERROR(getaddrinfo)
    }
        
    SOCKET socket_listen = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
    if (socket_listen == INVALID_SOCKET) {
        THROW_WIN32API_ERROR(socket);
    }
    
    if (bind(socket_listen, addr->ai_addr, (int)(addr->ai_addrlen)) != 0) {
        THROW_WIN32API_ERROR(bind);
    }

    if (listen(socket_listen, 1) != 0) {
        THROW_WIN32API_ERROR(listen);
    }

    int addrlen = sizeof(SOCKADDR_STORAGE);
    SOCKADDR_STORAGE sockaddr;
    SOCKET ret = accept(socket_listen, (SOCKADDR *)(&sockaddr), &addrlen);
    if (ret == INVALID_SOCKET) {
        THROW_WIN32API_ERROR(accept);
    }

    freeaddrinfo(addr);

    return ret;
}

#define BUFSIZE (1024 * 1024)

static void run(void)
{
    HANDLE ofile = OpenOutputFile(FileName.c_str());

    WSADATA data;
    WSAStartup(MAKEWORD(2, 0), &data);
    SOCKET socket = Listen(HostName.c_str(), Port.c_str());
    
    char *buf = new char[BUFSIZE];

    while (1) {
        int rdsize = recv(socket, buf, BUFSIZE, 0);
        if (rdsize == 0) {
            break;
        }
        if (rdsize == SOCKET_ERROR) {
            THROW_WIN32API_ERROR(recv);
        }
        DWORD wrsize;
        BOOL res;
        res = WriteFile(ofile, buf, (DWORD)rdsize, &wrsize, NULL);
        if (!res) {
            THROW_WIN32API_ERROR(WriteFile);
        }
        if ((DWORD)rdsize != wrsize) {
            THROW(UnknownError, "unknown error on data output.");
        }
        if (FileName != "") {
            res = FlushFileBuffers(ofile);
            if (!res) {
                THROW_WIN32API_ERROR(FlushFileBuffers);
            }
        }
    }
}

int main(int argc, char *argv[])
{
    try {
        parse_args(argc, argv);
        run();
    } catch (const CommandlineError &e) {
        fprintf(stderr, "Error:%s\n", e.what());
        print_usage();
        return 1;
    } catch (const ApplicationError &e) {
        fprintf(stderr, "Error:%s\n", e.what());
    }
    return 0;
}
