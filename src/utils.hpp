#ifndef NETRECV_UTILS_HPP
#define NETRECV_UTILS_HPP

#include <stdexcept>

#define ARYSIZE(a_) ((int)(sizeof(a_) / sizeof((a_)[0])))

#define THROW(class_, ...) {                    \
        const char *msg = format(__VA_ARGS__);  \
        class_ exp(msg);                        \
        delete [] msg;                          \
        throw exp;                              \
    }

#define THROW_WIN32API_ERROR(function_) {                               \
        char *errmsg;                                                   \
        DWORD errcode = GetLastError();                                 \
        FormatMessage(                                                  \
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, \
            NULL,                                                       \
            errcode,                                                    \
            MAKELANGID(LANG_NEUTRAL, SUBLANG_CUSTOM_DEFAULT),           \
            (LPTSTR)(&errmsg),                                          \
            0,                                                          \
            NULL);                                                      \
        const char *msg = format("error on %s:(%d) %s", #function_, errcode, errmsg); \
        Win32APIError exp(msg);                                         \
        delete [] msg;                                                  \
        LocalFree(errmsg);                                              \
        throw exp;                                                      \
    }

void usage_assert(int cond, const char *strcond, const char *fname, int line, const char *func, const char *fmt, ...);

#ifndef NDEBUG
#  define USAGE_ASSERT(cond_, ...) usage_assert((cond_), #cond_, __FILE__, __LINE__, __func__, __VA_ARGS__)
#else
#  define USAGE_ASSERT(cond_, ...) ((void)0)
#endif

#define NOT_IMPLEMENTED_YET                     \
    fprintf(stderr, "not implemented yet:%s:%d:%s\n", __FILE__, __LINE__, __func__), abort()

#define UNEXPECTED_STATE                        \
    fprintf(stderr, "unexpected state:%s:%d:%s\n", __FILE__, __LINE__, __func__), abort()

static inline int select_max(int x, int y)
{
    return (x > y) ? x : y;
}

static inline int select_min(int x, int y)
{
    return (y > x) ? x : y;
}

static inline int clamp(int v, int min, int max)
{
    return select_max(select_min(v, max), min);
}

#define UNREF(x_) ((void)(x_))

void trace(const char *fname, int line, const char *func, const char *fmt, ...);

#ifndef NDEBUG
#  define TRACE(...) trace(__FILE__, __LINE__, __func__, __VA_ARGS__);
#else
#  define TRACE(...) ((void)0)
#endif

#define PUTMSG(...) fprintf(stdout, __VA_ARGS__)

#define PUTERR(...) {                           \
        fprintf(stderr, "Error: ");             \
        fprintf(stderr, __VA_ARGS__);           \
        fprintf(stderr, "\n");                  \
    }

const char *vformat(const char *fmt, va_list ap);
const char *format(const char *fmt, ...);

class ApplicationError : std::exception {
public:
    ApplicationError(ApplicationError &e) {
        m_message = new std::string(e.what());
    }
    ApplicationError(const char *message) {
        m_message = new std::string(message);
    }
    ~ApplicationError() {
        delete m_message;
    }
    const char* what( void ) const noexcept {
        return m_message->c_str();
    }
protected:
    const std::string *m_message;
};

class Win32APIError : public ApplicationError {
public:
    using ApplicationError::ApplicationError;
    using ApplicationError::what;
};

#endif /* NETRECV_UTILS_HPP */
