#include "utils.hpp"
#include <cstdio>
#include <cstdarg>
#include <string>

const char *vformat(const char *fmt, va_list ap)
{
    va_list save_ap;
    va_copy(save_ap, ap);
    size_t size = 1 + vsnprintf(NULL, 0, fmt, ap);
    char *ret = new char[size];
    vsnprintf(ret, size, fmt, save_ap);
    va_end(save_ap);
    return ret;
}

const char *format(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    const char *ret = vformat(fmt, ap);
    va_end(ap);
    return ret;
}

void usage_assert(int cond, const char *strcond, const char *fname, int line, const char *func, const char *fmt, ...)
{
    if (!cond) {
        fprintf(stderr, "USAGE_ASSERT:%s:%d:%s:%s:", fname, line, func, strcond);
        va_list ap;
        va_start(ap, fmt);
        vfprintf(stderr, fmt, ap);
        va_end(ap);
        fprintf(stderr, "\n");
        fflush(stderr);
        abort();
    }
}

void trace(const char *fname, int line, const char *func, const char *fmt, ...)
{
    fprintf(stderr, "TRACE:%s:%d:%s:", fname, line, func);
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fflush(stderr);
}
