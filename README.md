# netrecv

## Summary

This is the application program for Windows.

This program receives the data stream from the network by TCP/IP and outputs to the file or standard-output.

## Usage

```
netrecv host-name port [file]
```

| argument  | description                                                                                                |
|-----------|------------------------------------------------------------------------------------------------------------|
| host-name | Host name or IP-Address to listen on.                                                                      |
| port      | Port number to listen on.                                                                                  |
| file      | File path which the received data stream be written to. When it is not specified, standard-output is used. |

